app_path = '/srv/www/wxm-preprod/wxm'
directory app_path
environment 'production'

ENV["HOME"] = app_path

threads 4,4

bind  "unix://#{app_path}/tmp/sockets/wxm.sock"
pidfile "#{app_path}/tmp/pids/puma.pid"
state_path "#{app_path}/tmp/sockets/puma.state"

activate_control_app

on_restart do
	puts 'Refreshing Gemfile'
	ENV["BUNDLE_GEMFILE"] = "#{app_path}/Gemfile"
end