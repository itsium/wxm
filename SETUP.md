### Install

	apt-get install imagemagick

	mkdir extensions
	cd extensions
	git clone git@git.itsium.cn:itsium/wxm_question.git
	git clone git@git.itsium.cn:itsium/wxm_bcard.git
	git clone git@git.itsium.cn:itsium/wxm_event.git

	cd ..
	bundle install
	sh install_extensions.sh

### Start server

	rails s

### Production

RAILS_ENV=production rake assets:clean && RAILS_ENV=production rake assets:precompile