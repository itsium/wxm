1. Puts every extensions in extensions folder
2. Use `rails plugin new wxm_myextension --full` to create a new extension
3. Create a main css and js file :

    app/assets/javascripts/wxm_myextension.js
    app/assets/stylesheets/wxm_myextension.css

4. copy the install_generator.rb from existent extenstions to

    wxm_myextension/lib/generators/wxm_myextension/install/install_generator.rb

    and modify the content to correspond to your project name
5. Use rails g wxm_myextension:install in the main wxm project directory to install migrations
    and include assets files.
