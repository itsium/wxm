class AddAppuidToWeiapps < ActiveRecord::Migration
  def change
    add_column :weiapps, :appuid, :string
    add_index :weiapps, :appuid, unique: true
  end
end
