class CreateWeiapps < ActiveRecord::Migration
  def change
    create_table :weiapps do |t|
      t.references :user, index: true
      t.integer :app_id, index: true
      t.string :app_type, index: true

      t.timestamps
    end
  end
end
