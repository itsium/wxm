class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :wx_openid
      t.string :nickname
      t.string :sex
      t.string :city
      t.string :country
      t.string :province
      t.string :language
      t.string :wx_headimgurl

      t.timestamps
    end
  end
end
