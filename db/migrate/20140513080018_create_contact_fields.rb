class CreateContactFields < ActiveRecord::Migration
  def change
    create_table :contact_fields do |t|
      t.integer :contactable_id, index: true
      t.string :contactable_type, index: true
      t.string :name
      t.string :value
      t.integer :ftype

      t.timestamps
    end
  end
end
