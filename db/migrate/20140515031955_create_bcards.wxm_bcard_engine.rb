# This migration comes from wxm_bcard_engine (originally 20140515024901)
class CreateBcards < ActiveRecord::Migration
  def change
    create_table :bcards do |t|
      t.references :weiapp, index: true
      t.string :name
      t.string :company
      t.string :jobtitle
      t.string :theme
      t.string :vcardurl

      t.timestamps
    end
  end
end
