#!/bin/sh
CUR_PATH=$PWD
for i in `ls extensions`
do
    echo "[+] Installing "$i
    yes | rails g $i:install
done
