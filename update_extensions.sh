#!/bin/sh
CUR_PATH=$PWD
for i in `ls extensions`
do
    echo "[+] Updating "$i
    cd "extensions/"$i
    git pull
    cd $CUR_PATH
done
