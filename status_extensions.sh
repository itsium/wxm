#!/bin/sh
CUR_PATH=$PWD
for i in `ls extensions`
do
    echo "[i] Status "$i
    cd "extensions/"$i
    git status
    cd $CUR_PATH
done
