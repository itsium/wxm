# == Schema Information
#
# Table name: weiapps
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  app_id     :integer
#  app_type   :string(255)
#  created_at :datetime
#  updated_at :datetime
#  appuid     :string(255)
#

class Weiapp < ActiveRecord::Base
  belongs_to :user
  belongs_to :app, :polymorphic => true

  before_create :genappuid

  def self.get_icon(type)
    if @icons.nil?
      @icons = {
        bcard: 'fa-user',
        event: 'fa-envelope',
        question: 'fa-question-circle'
      }
    end
    @icons[type]
  end

  def icon
  end

  def genappuid
    uid = ""
    begin
        uid = randomuid
    end while Weiapp.where(appuid: uid).count != 0
    self.appuid = uid
  end

    def randomuid(length=6)
      chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789'
      seed = ''
      length.times { seed << chars[rand(chars.size)] }
      seed
    end
end
