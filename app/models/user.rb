# == Schema Information
#
# Table name: users
#
#  id            :integer          not null, primary key
#  wx_openid     :string(255)
#  nickname      :string(255)
#  sex           :string(255)
#  city          :string(255)
#  country       :string(255)
#  province      :string(255)
#  language      :string(255)
#  wx_headimgurl :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class User < ActiveRecord::Base
    has_many :weiapps

    def as_json(options=nil)
        item = super({ only: [
            :id, :nickname, :language
        ] }.merge(options || {}))
        item.merge({
            avatar: self.wx_headimgurl
        })
    end

end
