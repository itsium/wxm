# == Schema Information
#
# Table name: qrcodes
#
#  id                 :integer          not null, primary key
#  url                :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

require 'rqrcode'

class Qrcode < ActiveRecord::Base
	has_attached_file :image
	do_not_validate_attachment_file_type :image
	before_save :update_qrcode

	private

		def update_qrcode
			self.image = create_qrcode(self.url)
		end

		def create_qrcode(string, options={})
			qrcode = RQRCode::QRCode.new(string)
			png = qrcode.as_png({ resize_exactly_to: 200 })
			image = MiniMagick::Image.read png.to_blob
			image.format "png"
			File.open(image.path)
		end
end
