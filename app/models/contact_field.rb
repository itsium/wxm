# == Schema Information
#
# Table name: contact_fields
#
#  id               :integer          not null, primary key
#  contactable_id   :integer
#  contactable_type :string(255)
#  name             :string(255)
#  value            :string(255)
#  ftype            :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class ContactField < ActiveRecord::Base
    extend Enumerize
    enumerize :ftype, in: {:phone => 0 , :email => 1 , :mobile => 2 , :address => 3 , :website => 4 ,
                                    :fax => 5 , :social_network => 6 , :weixin => 7 , :weibo => 8 , :qq => 9 }
    belongs_to :contactable, polymorphic: true

    def as_json(options=nil)
        item = super({ only: [
            :name, :value, :ftype
        ] }.merge(options || {}))
      end

end
