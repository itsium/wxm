class HomeController < ApplicationController
  before_filter :wx_auth, except: [:wxlogin, :mobileshow]
  layout "mobile", only: [ :mobileshow ]

  def index
    @str = cur_user_agent
    @is_wechat = is_wechat
  end

  def mobileshow
    @weiapp = Weiapp.where(appuid: params[:appuid]).first
    if @weiapp.nil?
      render_404
    else
      redirect_to controller: @weiapp.app.class.model_name.plural, action: "mobileshow"
    end
  end

  def allapps
    @apps = current_user.weiapps.page(params[:page]).per(3)
    @total_apps = current_user.weiapps.count
  end

  def logout
    cookies[:user_openid] = nil
    redirect_to root_url, notice: "Logout successfull !"
  end

  def wxuser
  end

  def openinbrowser
    @is_wechat = is_wechat
    if ! @is_wechat
       @user = User.where(wx_openid: params[:openid]).first
       cookies[:user_openid] = {
          value: @user.wx_openid,
          expires: 1.week.from_now
        }
        redirect_to params[:redirection], flash: { success: "Hello #{@user.nickname} !" }
    end
  end

  def mobilewxlogin
    if current_user
      redirect_to '/' + params[:redirection_url]
    else
      wx_auth(params[:redirection_url])
    end
  end

  def wxlogin
    if is_wechat
        userinfo = get_userinfo(params[:code])
        @user = User.where(wx_openid: userinfo["openid"]).first

        if @user.nil?
          @user = User.create!(
              wx_openid:   userinfo["openid"],
              nickname:   userinfo["nickname"],
              sex:   userinfo["sex"],
              city:   userinfo["city"],
              country:   userinfo["country"],
              province:   userinfo["province"],
              language:  userinfo["language"],
              wx_headimgurl:   userinfo["headimgurl"]
            )
        end

        cookies[:user_openid] = {
          value: @user.wx_openid,
          expires: 1.week.from_now
        }
        if is_iphone
          redirect_to params[:state], flash: { success: "Hello #{@user.nickname} !" }
        elsif is_android
          redirect_to openinbrowser_url(openid: @user.wx_openid, redirection: params[:state])
        end
        # TODO Redirect to root with error message
        logger.debug "[INFO] Not a wechat user"
    end
  end


  def new_app
    #TODO Make a gem per App list all availble apps, and get ressources from each gems (name icon description etc),
    @app_availables = [{
      new_app_url:  new_bcard_url,
      name: "Business Card",
      description: "Business card app give you the possiblility to create social network shareble id card.",
      icon: Weiapp.get_icon(:bcard),
      enabled: true
    }, {
      new_app_url:  new_question_url,
      name: "Questions",
      description: "2 answers question",
      icon: Weiapp.get_icon(:question),
      enabled: true
    }, {
      new_app_url:  new_event_url,
      name: "Invitation",
      description: "Interactive invitation",
      icon: Weiapp.get_icon(:event),
      enabled: true
    }]
  end
end
