class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :cur_user_agent
  before_filter :debug_qrcode
  before_filter :check_wechat
  helper_method :current_user


protected
  def current_user
    if Rails.env.development? && is_wechat == false
      return @current_user = User.first
    end
    return unless cookies[:user_openid]
    @current_user ||= User.where(wx_openid: cookies[:user_openid]).first
  end

  def check_user
       redirect_to root_url, flash: {error: t(:not_logged)}  if current_user.nil?
  end

  def check_wechat
    if Rails.env.development?
      return
    end
    if request.original_fullpath == "/not_weixin" and Rails.env.production?
      @qr = gen_qrcode(request.original_url)
    end
    if request.original_fullpath != "/not_weixin" and self.is_wechat == false
      redirect_to "/not_weixin"
    elsif request.original_fullpath == "/not_weixin" and self.is_wechat == true
      redirect_to "/n"
    end
  end

  def cur_user_agent
      @cur_user_agent ||= UserAgent.parse(request.user_agent)
  end

  def is_wechat
      (cur_user_agent.to_s =~ /MicroMessenger/i) != nil
  end

  def is_iphone
      (cur_user_agent.to_s =~ /iPhone/i) != nil
  end

  def is_android
      (cur_user_agent.to_s =~ /Android/i) != nil
  end

  def get_userinfo(code)
      response = HTTParty.get("https://api.weixin.qq.com/sns/oauth2/access_token?appid=#{Rails.configuration.wxappid}&secret=#{Rails.configuration.wxappsecret}&code=#{code}&grant_type=authorization_code")
      # $expires_in = response.parsed_response["expires_in"]
      # $accesstoken_time = Time.now.to_i + ($expires_in - 30) # 30 second is the buffer time
      datajson = JSON.parse(response.parsed_response)
      accesstoken = datajson["access_token"]
      openid = datajson["openid"]
      response = HTTParty.get("https://api.weixin.qq.com/sns/userinfo?access_token=#{accesstoken}&openid=#{openid}&lang=zh_CN")
      JSON.parse(response.parsed_response)
  end

  def wx_auth(redirection_url = nil)
    if current_user.nil?
      wx_appip = "wx7c35f10103e31584"
      callback_url = "http://w42.itsium.cn#{wxlogin_path}"
      #callback_url = "#{request.protocol}#{request.host_with_port}#{wxlogin_path}"
      if not redirection_url.nil?
        redirection_url = "#{request.protocol}#{request.host}/#{redirection_url}"
      end
      redirection_url = request.original_url if redirection_url.nil?
      puts redirection_url
      #puts callback_url
      #puts redirection_url
      if is_wechat
        redirect_to "https://open.weixin.qq.com/connect/oauth2/authorize?appid=#{wx_appip}&redirect_uri=#{Rack::Utils.escape(callback_url)}&response_type=code&scope=snsapi_userinfo&state=#{Rack::Utils.escape(redirection_url)}#wechat_redirect"
      end
    end
  end

  def debug_qrcode
    if Rails.env.development?
      @qr = gen_qrcode(request.original_url)
    end
  end

  def gen_qrcode(str)
    qrcode = Qrcode.where(url: str).first()

    if qrcode.nil?
      qrcode = Qrcode.new({
        url: str
      })
      qrcode.save
    end

    qrcode
    # reload = false
    # qr_size = 2
    # qr = nil
    # until qr
    #   begin
    #     qr = RQRCode::QRCode.new(str, :size => qr_size)
    #   rescue Exception=>e
    #     reload = true
    #     qr_size += 1
    #   end
    # end
    # qr
  end



  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

end
