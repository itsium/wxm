# == Schema Information
#
# Table name: contact_fields
#
#  id               :integer          not null, primary key
#  contactable_id   :integer
#  contactable_type :string(255)
#  name             :string(255)
#  value            :string(255)
#  ftype            :integer
#  created_at       :datetime
#  updated_at       :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact_field do
    contactable_id 1
    contactable_type "MyString"
    name "MyString"
    value "MyString"
    ftype 1
  end
end
