# == Schema Information
#
# Table name: users
#
#  id            :integer          not null, primary key
#  wx_openid     :string(255)
#  nickname      :string(255)
#  sex           :string(255)
#  city          :string(255)
#  country       :string(255)
#  province      :string(255)
#  language      :string(255)
#  wx_headimgurl :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    wx_openid "MyString"
    nickname "MyString"
    sex "MyString"
    city "MyString"
    country "MyString"
    province "MyString"
    language "MyString"
    wx_headimgurl "MyString"
  end
end
