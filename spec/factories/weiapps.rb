# == Schema Information
#
# Table name: weiapps
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  app_id     :integer
#  app_type   :string(255)
#  created_at :datetime
#  updated_at :datetime
#  appuid     :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :weiapp do
    user nil
    app_id 1
    app_type "MyString"
  end
end
