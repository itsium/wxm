source 'https://ruby.taobao.org/'
#source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.5'

# Use sqlite3 as the database for Active Record
gem 'sqlite3'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'
gem 'jpbuilder'
gem 'devise'
gem 'activeadmin', github: 'gregbell/active_admin'
gem 'puma'
gem 'therubyracer', :platform=>:ruby
gem 'paperclip', github: 'thoughtbot/paperclip'
gem 'capistrano', '~> 3.2.0'
gem 'capistrano-rails', '~> 1.1'
gem 'capistrano-puma', require: false
gem 'mini_magick'
#gem 'rqrcode', github: 'itsium/rqrcode'
gem 'rqrcode-with-patches'
gem 'useragent'
gem 'formtastic'
gem 'formtastic-bootstrap'
gem 'wxm_bcard', :path => "extensions/wxm_bcard"
gem 'wxm_question', :path => "extensions/wxm_question"
gem 'wxm_event', :path => "extensions/wxm_event"
gem 'httparty'
gem 'enumerize'
gem 'kaminari-bootstrap', '~> 3.0.1'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
group :development do
  gem 'hub', :require=>nil
  gem 'rails_layout'
  gem "spring"
  gem 'byebug'
  gem 'annotate', :git => 'git://github.com/ctran/annotate_models.git'
  gem 'guard-livereload', require: false
  gem "rack-livereload"
  gem "guard-rails"
  gem "guard-bundler"
  gem 'quiet_assets'
  gem "better_errors"
  gem "binding_of_caller"
end
group :development, :test do
  gem 'factory_girl_rails'
  gem 'rspec-rails'
  gem "rb-readline", "~> 0.5.0"
  gem 'guard-rspec', '2.5.0'
end
group :test do
  gem 'database_cleaner', '1.0.1'
  gem 'email_spec'
  gem 'shoulda-matchers'

  # Uncomment this line on OS X.
  # gem 'growl', '1.0.3'

  # Uncomment these lines on Linux.
  gem 'libnotify', '0.8.0'

  # Uncomment these lines on Windows.
  # gem 'rb-notifu', '0.0.4'
  # gem 'win32console', '1.3.2'
  # gem 'wdm', '0.1.0'
end